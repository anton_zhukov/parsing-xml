package com.dev.bankdeposit.exception;


public class WrongChoiceException extends Exception {
    public WrongChoiceException() {
    }

    public WrongChoiceException(String message) {
        super(message);
    }

    public WrongChoiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongChoiceException(Throwable cause) {
        super(cause);
    }
}
