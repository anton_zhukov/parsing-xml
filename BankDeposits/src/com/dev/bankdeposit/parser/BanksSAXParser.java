package com.dev.bankdeposit.parser;

import com.dev.bankdeposit.entity.Investment;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.Set;

public class BanksSAXParser implements Parser {

    private static final Logger LOG = Logger.getLogger(BanksSAXParser.class);

    private Set<Investment> investments;

    public BanksSAXParser(String banksXmlFileName) {
        parseDocument(banksXmlFileName);
    }

    private void parseDocument(String banksXmlFileName) {
        BanksSAXHandler bankSAXHandler = new BanksSAXHandler();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(banksXmlFileName, bankSAXHandler);
        } catch (ParserConfigurationException e) {
            LOG.error("Parser Configuration error");
        } catch (SAXException e) {
            LOG.error("SAXException : xml not well formed");
        } catch (IOException e) {
            LOG.error("IOException", e);
        }
        investments = bankSAXHandler.getInvestments();
    }

    @Override
    public Set<Investment> getInvestments() {
        return investments;
    }
}
