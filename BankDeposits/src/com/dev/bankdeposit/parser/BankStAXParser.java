package com.dev.bankdeposit.parser;

import com.dev.bankdeposit.entity.*;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

public class BankStAXParser implements Parser {

    private static final Logger LOG = Logger.getLogger(BankStAXParser.class);

    private Set<Investment> investments;
    private RoubleDeposit roubleDeposit;
    private CurrencyDeposit currencyDeposit;
    private AmountOnDeposit amountOnDeposit;
    private Insurance insurance;
    private String tagContent;
    private XMLInputFactory factory;
    private boolean isCurrencyDeposit;
    private boolean isInInsurance;

    public BankStAXParser(String banksXmlFileName) {
        factory = XMLInputFactory.newInstance();
        parseDocument(banksXmlFileName);
    }

    private void parseDocument(String banksXmlFileName) {
        File file = new File(banksXmlFileName);
        XMLStreamReader reader;
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(file);
            reader = factory.createXMLStreamReader(fileInputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT:
                        switch (reader.getLocalName()) {
                            case ParsingConstants.ROUBLE_DEPOSIT:
                                isCurrencyDeposit = false;
                                roubleDeposit = new RoubleDeposit();
                                roubleDeposit.setAccountId(Integer.parseInt(reader.getAttributeValue(0).substring(1)));
                                break;
                            case ParsingConstants.CURRENCY_DEPOSIT:
                                isCurrencyDeposit = true;
                                currencyDeposit = new CurrencyDeposit();
                                currencyDeposit.setAccountId(Integer.parseInt(reader.getAttributeValue(0).substring(1)));
                                break;
                            case ParsingConstants.AMOUNT_ON_DEPOSIT:
                                isInInsurance = false;
                                amountOnDeposit = new AmountOnDeposit();
                                break;
                            case ParsingConstants.INSURANCE:
                                isInInsurance = true;
                                insurance = new Insurance();
                                break;
                            case ParsingConstants.BANKS:
                                investments = new HashSet<>();
                                break;
                        }
                        break;

                    case XMLStreamConstants.CHARACTERS:
                        tagContent = reader.getText().trim();
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        if (!isCurrencyDeposit) {
                            buildInvestment(roubleDeposit, reader.getLocalName());
                            switch (reader.getLocalName()) {
                                case ParsingConstants.PROLONGATION_ABILITY:
                                    roubleDeposit.setProlongationAbility(Boolean.parseBoolean(tagContent));
                                    break;
                                case ParsingConstants.CURRENCY:
                                    amountOnDeposit.setCurrency(tagContent);
                                    break;
                                case ParsingConstants.AMOUNT:
                                    amountOnDeposit.setAmount(Double.parseDouble(tagContent));
                                    break;
                                case ParsingConstants.ROUBLE_DEPOSIT:
                                    investments.add(roubleDeposit);
                                    break;
                            }
                        } else {
                            buildInvestment(currencyDeposit, reader.getLocalName());
                            switch (reader.getLocalName()) {
                                case ParsingConstants.REPLENISHMENT:
                                    currencyDeposit.setReplenishment(Boolean.parseBoolean(tagContent));
                                    break;
                                case ParsingConstants.PERIOD_TO_PAY:
                                    insurance.setPeriodToPay(tagContent);
                                    break;
                                case ParsingConstants.CURRENCY:
                                    if (isInInsurance) {
                                        insurance.setCurrency(tagContent);
                                    } else {
                                        amountOnDeposit.setCurrency(tagContent);
                                    }
                                    break;
                                case ParsingConstants.AMOUNT:
                                    if (isInInsurance) {
                                        insurance.setAmount(Double.parseDouble(tagContent));
                                    } else {
                                        amountOnDeposit.setAmount(Double.parseDouble(tagContent));
                                    }
                                    break;
                                case ParsingConstants.INSURANCE:
                                    currencyDeposit.setInsurance(insurance);
                                    break;
                                case ParsingConstants.CURRENCY_DEPOSIT:
                                    investments.add(currencyDeposit);
                                    break;
                            }
                        }
                        break;
                }
            }
        } catch (XMLStreamException e) {
            LOG.error("XMLStreamException", e);
        } catch (FileNotFoundException e) {
            LOG.error("FileNotFoundException", e);
        }
    }

    private void buildInvestment(Investment investment, String localName) {
        switch (localName) {
            case ParsingConstants.NAME:
                investment.setName(tagContent);
                break;
            case ParsingConstants.COUNTRY:
                investment.setCountry(tagContent);
                break;
            case ParsingConstants.TYPE:
                investment.setDepositType(DepositTypes.fromString(tagContent));
                break;
            case ParsingConstants.DEPOSITOR:
                investment.setDepositor(tagContent);
                break;
            case ParsingConstants.AMOUNT_ON_DEPOSIT:
                investment.setAmountOnDeposit(amountOnDeposit);
                break;
            case ParsingConstants.PROFITABILITY:
                investment.setProfitability(Double.parseDouble(tagContent));
                break;
            case ParsingConstants.TIME_CONSTRAINTS:
                investment.setTimeConstraints(tagContent);
                break;
        }
    }

    @Override
    public Set<Investment> getInvestments() {
        return investments;
    }
}
