package com.dev.bankdeposit.function;

import com.dev.bankdeposit.entity.Investment;
import com.dev.bankdeposit.exception.WrongChoiceException;
import com.dev.bankdeposit.parser.BankStAXParser;
import com.dev.bankdeposit.parser.BanksDOMParser;
import com.dev.bankdeposit.parser.BanksSAXParser;
import com.dev.bankdeposit.parser.Parser;
import org.apache.log4j.Logger;

import java.util.Scanner;
import java.util.Set;

public class Demonstration {

    private static final Logger LOG = Logger.getLogger(Demonstration.class);

    private static final String FILE_NAME = "BankDeposits/banks.xml";
    private static final String SCHEMA_NAME = "BankDeposits/banks.xsd";

    public static void demonstrate() {
        Scanner scanner = new Scanner(System.in);

        LOG.info("Choose a parser: 1-SAX, 2-DOM, 3-StAX");

        try {
            Demonstration.doAction(scanner.nextInt());
        } catch (WrongChoiceException e) {
            LOG.error("Check chosen option", e);
        }
    }

    private static void doAction(int chosenParser) throws WrongChoiceException {

        boolean xmlIsValid = DepositValidator.validateFile(SCHEMA_NAME, FILE_NAME);

        if (xmlIsValid) {
            switch (chosenParser) {
                case 1:
                    LOG.info("Your choice: SAXParser");
                    Parser parserSax = new BanksSAXParser(FILE_NAME);
                    Set<Investment> investmentsSax = parserSax.getInvestments();
                    printInvestments(investmentsSax);
                    break;
                case 2:
                    LOG.info("Your choice: DOMParser");
                    Parser parserDom = new BanksDOMParser(FILE_NAME);
                    Set<Investment> investmentsDom = parserDom.getInvestments();
                    printInvestments(investmentsDom);
                    break;
                case 3:
                    LOG.info("Your choice: StAXParser");
                    Parser parserStax = new BankStAXParser(FILE_NAME);
                    Set<Investment> investmentsStax = parserStax.getInvestments();
                    printInvestments(investmentsStax);
                    break;
                default:
                    throw new WrongChoiceException("You have chosen wrong option!");
            }
        } else {
            LOG.error("Xml is not valid!");
        }
    }

    private static void printInvestments(Set<Investment> investmentSet) {
        for (Investment investment : investmentSet) {
            LOG.info(investment.toString());
        }
    }
}
