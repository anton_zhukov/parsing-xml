package com.dev.bankdeposit.entity;

public class Insurance {

    private String currency;
    private double amount;
    private String periodToPay;

    public Insurance() {
    }

    public Insurance(String currency, double amount, String periodToPay) {
        this.currency = currency;
        this.amount = amount;
        this.periodToPay = periodToPay;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPeriodToPay() {
        return periodToPay;
    }

    public void setPeriodToPay(String periodToPay) {
        this.periodToPay = periodToPay;
    }

    @Override
    public String toString() {
        return "insurance: " +
                "currency='" + currency + '\'' +
                ", amount=" + amount +
                ", periodToPay='" + periodToPay + '\'';
    }
}
