package com.dev.bankdeposit.entity;

public class RoubleDeposit extends Investment {

    private boolean prolongationAbility;

    public boolean isProlongationAbility() {
        return prolongationAbility;
    }

    public RoubleDeposit setProlongationAbility(boolean prolongationAbility) {
        this.prolongationAbility = prolongationAbility;
        return this;
    }

    @Override
    public String toString() {
        return "\nRoubleDeposit: \n" + super.toString()
                + "prolongationAbility=" + prolongationAbility + "\n";
    }
}




