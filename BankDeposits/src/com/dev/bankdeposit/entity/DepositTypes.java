package com.dev.bankdeposit.entity;


public enum DepositTypes {

    TILL_CALLED_FOR("Till called for"),
    FIXED("Fixed"),
    OPERATING("Operating"),
    ACCUMULATIVE("Accumulative"),
    SAVINGS("Savings"),
    METALLIC("Metallic");

    private String text;

    DepositTypes(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public static DepositTypes fromString(String text) {
        if (text != null) {
            for (DepositTypes type : DepositTypes.values()) {
                if (text.equalsIgnoreCase(type.text)) {
                    return type;
                }
            }
        }
        return null;
    }
}