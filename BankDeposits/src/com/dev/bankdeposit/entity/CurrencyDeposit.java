package com.dev.bankdeposit.entity;

public class CurrencyDeposit extends Investment {

    private boolean replenishment;
    private Insurance insurance;

    public boolean isReplenishment() {
        return replenishment;
    }

    public CurrencyDeposit setReplenishment(boolean replenishment) {
        this.replenishment = replenishment;
        return this;
    }

    public Insurance getInsurance() {
        return insurance;
    }

    public CurrencyDeposit setInsurance(Insurance insurance) {
        this.insurance = insurance;
        return this;
    }

    @Override
    public String toString() {
        return "\nCurrencyDeposit: \n" + super.toString()
                + "replenishment=" + replenishment + "\n"
                + insurance + "\n";
    }
}
