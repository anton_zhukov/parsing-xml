package com.dev.bankdeposit.main;

import com.dev.bankdeposit.function.Demonstration;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;

public class Runner {

    static {
        new PropertyConfigurator().doConfigure("properties/log4j.properties", LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        Demonstration.demonstrate();
    }
}
